from django.test import TestCase, Client
from rest_framework import status
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from django.conf import settings
import json
import logging

logger = logging.getLogger('apirest_gpio')

# API Authentication
try:
    user = User.objects.get(username='admin')
except:
    logger.error("Login para test falló")
    exit(-1)
client = APIClient()
client.force_authenticate(user=user)


# Create your tests here.
class GPIOTest(TestCase):
    # def setUp(self):
    #     self.valid_payload = {
    #         'value': "HIGH"
    #     }
    #     self.invalid_payload = {
    #         'kk': 'tutua'
    #     }

    def test_pin_info(self):
        url = '/api/v1/gpio/'
        response = client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['result']), 40)

        # Comprobamos primer pin tiene los atributos mode y value
        logger.debug(response.data)
        claves =response.data['result']['1'].keys()
        result_1 = response.data['result']['1']
        if result_1['mode'] is "IN":
            self.assertSetEqual(set(claves), set(("mode", "value", "pull_ud")))
            self.assertEqual(result_1['pull_ud'], "UNKNOW")
        else:
            self.assertSetEqual(set(claves), set(("mode", "value")))


    def test_valid_out_mode(self):
        url = '/api/v1/gpio/out/17/'
        valid_payload = {
            "value": 1
        }

        # TEST método PUT
        response = client.put(url, data=valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['result'], {"mode": "OUT", "value": 1})

        # TEST método GET
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['result'], {"mode": "OUT", "value": 1})


    def test_invalid_out_mode(self):
         url='/api/v1/gpio/out/17/'
         invalid_payload = {
             'tutua': 'tumetata'
         }
         response = client.put(url, data=invalid_payload,format='json')
         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_valid_in_mode(self):
        url = '/api/v1/gpio/in/27/?pull_ud=PUD_DOWN'

        response=client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        claves = response.data['result'].keys()
        self.assertSetEqual(set(claves), set(("mode", "pull_ud", "value")))
        self.assertEqual(response.data['result']['mode'], "IN")
        self.assertEqual(response.data['result']['pull_ud'], "PUD_DOWN")
        self.assertIn(response.data['result']['value'], set((1, 0)))

        url = '/api/v1/gpio/in/27/?pull_ud=PUD_UP'
        response=client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        claves =response.data['result'].keys()
        self.assertSetEqual(set(claves), set(("mode", "pull_ud", "value")))
        self.assertEqual(response.data['result']['mode'], "IN")
        self.assertEqual(response.data['result']['pull_ud'], "PUD_UP")
        self.assertIn(response.data['result']['value'], set((1, 0)))
