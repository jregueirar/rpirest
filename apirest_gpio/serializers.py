from rest_framework import serializers
import RPi.GPIO as GPIO

# Solo los soportados
GPIO_MODES = (
    ("IN", GPIO.IN),
    ("OUT", GPIO.OUT),
)

GPIO_PIN_VALUE = (
    ("HIGH", GPIO.HIGH),
    ("LOW", GPIO.LOW),
)

PULL_UP_RESISTOR = (
    ("PUD_DOWN", GPIO.PUD_DOWN),
    ("PUD_OFF", GPIO.PUD_OFF),
    ("PUD_UP", GPIO.PUD_UP)
)

class PinSerializer(serializers.Serializer):
    mode = serializers.ChoiceField(choices=GPIO_MODES)
    value = serializers.ChoiceField(choices=GPIO_PIN_VALUE)


class ModeOutSerializer(serializers.Serializer):
    value = serializers.IntegerField()


class ModeINSerializer(serializers.Serializer):
    pull_ud = serializers.ChoiceField(choices=PULL_UP_RESISTOR)
    # value = serializers.ChoiceField(choices=GPIO_PIN_VALUE)

