from django.apps import AppConfig


class ApirestGpioConfig(AppConfig):
    name = 'apirest_gpio'
