from rest_framework.response import Response
from rest_framework import viewsets, status
from django.conf import settings
from core.common import MyRouter
from core.common import apirest_response_format, rpirest_response
from apirest_gpio.serializers import ModeOutSerializer, PinSerializer, ModeINSerializer
import sys

import logging
logger = logging.getLogger("apirest_gpio")

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


def routes():

    router = MyRouter()
    # out, in son los modos
    router.register(r'', GPIOView, base_name='gpio')
    router.register(r'out', GPIOOutView, base_name='gpio_out')
    router.register(r'in', GPIOInView, base_name='gpio_in')
    return router.urls

gpiomode_int2str = {
    GPIO.IN: "IN",
    GPIO.OUT: "OUT",
    GPIO.SPI: "SPI",
    GPIO.I2C: "I2C",
    GPIO.HARD_PWM:"PWM",
    GPIO.SERIAL: "SERIAL",
    GPIO.UNKNOWN: "UNKNOWN"
}

pull_ud_int2str ={
    GPIO.PUD_DOWN: "PUD_DOWN",
    GPIO.PUD_OFF: "PUD_OFF",
    GPIO.PUD_UP: "PUD_UP"
}

pull_ud_str2int = {
    "PUD_DOWN": GPIO.PUD_DOWN,
    "PUD_OFF": GPIO.PUD_OFF,
    "PUD_UP": GPIO.PUD_UP
}



def led_is_lit(pin):
    GPIO.setup(pin, GPIO.OUT)
    if GPIO.input(pin):
        return True
    else:
        return False

# Devuelve un hash con la siguiente informacion:
# {
#   mode: "IN, OUT,..."
#   value:
# }
def pin_info(pin, mode=None, pull_ud=None):
    result = {}

    if mode is None:
        mode = GPIO.gpio_function(pin)
    result['mode'] = gpiomode_int2str[mode]

    if mode is GPIO.OUT:
        GPIO.setup(pin, GPIO.OUT)
        result['value'] = GPIO.input(pin)
    elif mode is GPIO.IN:
        if pull_ud is None:
            GPIO.setup(pin, GPIO.IN)
        else:
            GPIO.setup(pin, GPIO.IN, pull_up_down=pull_ud)
        result['value'] = GPIO.input(pin)
        if pull_ud is None:
            result['pull_ud'] = "UNKNOW"
        else:
            result['pull_ud'] = pull_ud_int2str[pull_ud]
    else:
        result['value'] = "mode unknow"

    logger.debug(result)
    return result


class GPIOView(viewsets.ViewSet):
    # serializer_class = PinSerializer

    def list(self, request):
        try:
            result={}
            i = 1
            while i <= 40:
                result[str(i)] = pin_info(i)
                i = i + 1
            logger.debug(result)
            return rpirest_response(request=request, status=status.HTTP_200_OK, result=result)
        except:
            return rpirest_response(request=request, status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                    result=None, detail=sys.exc_info()[1])

    def retrieve(self, request, pk=None):
        try:
            pin = int(pk)
            result = pin_info(pin)
            return rpirest_response(request=request, status=status.HTTP_200_OK, result=result)
        except:
            return rpirest_response(request=request, status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                             result=None, detail=sys.exc_info()[1])




class GPIOOutView(viewsets.ViewSet):
    serializers_class = ModeOutSerializer
    basename = "gpio_out"

    def retrieve(self, request, pk=None):
        """
        Devuelve información acerca de un pin. Ejemplo:
         "mode":    "undefined",
         "pull":    "off",
         "value":    0,
         "frequency":    1000,
         "range":    100
        """
        logger.debug("Entro GPIOVIEW")
        if pk is not None:
            pin = int(pk)
            logger.debug("Pin: " + str(pin))

            # FIXME hacer un GPIO_SERIALIZER y chequear que es un pin de proposito general válido.
            # Como se hace con XYSERIALIZER
            # Umm hay que leer tb el valor.
            try:
                result = pin_info(pin, mode=GPIO.OUT)
                return rpirest_response(request=request, status=status.HTTP_200_OK, result=result)
            except:
                return rpirest_response(request=request,
                                  status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                 result=None, detail=sys.exc_info()[1])
        else:
            return rpirest_response(request=request,
                             status=status.HTTP_400_BAD_REQUEST, result=None)

    def update_element(self, request, pk=None):
        self.serializers_class = ModeOutSerializer

        serializer = ModeOutSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                pin = int(pk)
                self.pin_set_value(pin, serializer.data['value'])
                result = pin_info(pin, mode=GPIO.OUT)
                logger.debug(result)
                return rpirest_response(request=request,
                                        status=status.HTTP_200_OK,
                                        result=result)
            except:
                logger.debug("error 500")
                return rpirest_response(request=request,
                                        status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                        result=None, detail=sys.exc_info()[1])
        else:
            return rpirest_response(request=request,
                                    status=status.HTTP_400_BAD_REQUEST,
                                    result="", detail=serializer.errors)

    def pin_set_value(self, pin, value):
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, value)

class GPIOInView(viewsets.ViewSet):

    def retrieve(self, request, pk=None):
        if request.query_params:
            serializer = ModeINSerializer(data=request.query_params)
        else:
            serializer = ModeINSerializer(data=request.data)

        if pk is not None and serializer.is_valid(raise_exception=True):
            pin = int(pk)
            try:
                logger.debug(serializer.data['pull_ud'])
                pull_resistor = pull_ud_str2int[serializer.data['pull_ud']]
                result = pin_info(pin, mode=GPIO.IN, pull_ud=pull_resistor)
                return rpirest_response(request=request, status=status.HTTP_200_OK, result=result)
            except:
                return rpirest_response(request=request,
                                  status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                 result=None, detail=sys.exc_info()[1])
        else:
            return rpirest_response(request=request,
                             status=status.HTTP_400_BAD_REQUEST, result=None)
