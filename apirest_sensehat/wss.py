from celery import shared_task

class Show(viewsets.ViewSet):
    """
    Example of a AsyncJob. Sleep for X seconds.
    """
    serializer_class = DelaySerializer

    def update(self, request):
        serializer = DelaySerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            task = sleep.delay(serializer.data['time'])
            job = Job(
                name="sleep",
                celery_id=task.id
            )
            job.save()
            # FIXME sync_job_db

            msg_out="Async Job for testing. Sleep for X Seconds."
            response = apirest_response_format(request=request,
                                               status=task.status,
                                               msg=msg_out,
                                               result="",
                                               job_id=job.id,
                                               )
            return Response(response)


sense.show_message(request.data['text_string'],
                               scroll_speed = serializer.data['scroll_speed'],
                               text_colour = hash_colour_2_list(serializer.data['text_colour']),
                               back_colour = hash_colour_2_list(serializer.data['back_colour'])
                               )
