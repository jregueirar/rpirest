from celery import shared_task
from django.conf import settings
from .utils import hash_colour_2_list

# ef hash_colour_2_list(hash):
#     return [hash['r'], hash['g'], hash['b']]
if settings.IS_RPI:
    try:
        from sense_hat import SenseHat
    except ImportError:
        raise SystemExit('[ERROR] Please make sure sense_hat is installed properly')

    try:
        # logger.debug("Initializing sense")
        sense = SenseHat()
    except OSError:
        # logger.error("[ERROR] Initializing sensehat. Please make sure sense_hat is installed properly")
        # Escribimos en base de datos status o en el render...¿Decorador para cada vista?.
        pass

@shared_task
def show_message(data):
    sense.show_message(data['text_string'],
                        data['scroll_speed'],
                        text_colour=hash_colour_2_list(data['text_colour']),
                        back_colour=hash_colour_2_list(data['back_colour'])
                       )
