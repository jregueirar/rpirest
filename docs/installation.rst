====================
Instalación 
====================

1. Primero, instala Raspbian con acceso ssh. Comprueba que puedes acceder por ssh a la Raspberry Pi. 

2. Segundo, haz el despliegue de RPiREST con ansible

.. code-block:: console
 
  git clone https://github.com/jregueirar/ansible-rpirest
  ansible-galaxy install -r requiriments.yml
  #Edit inventory file with your Raspberrypi infraestructure
  ansible-playbook pirest-sense-hat.yml -l rpis -i inventory

3. Tercero, crea un super usuario para poder hacer login en la interfaz web

.. code-block:: console
  
  ssh rpi
  cd /opt/rpirest
  python3 manage.py createsuperuser --username admin --email admin@localhost --noinput
  python3 manage.py changepassword admin
  ```

4. Por último comprueba que puedes hacer login en la interfaz web

.. _Raspbian: https://www.raspberrypi.org/downloads/raspbian/
.. _raspberrypi.org: https://www.raspberrypi.org/downloads/
