Requisitos
============

El proyecto RPiREST ha sido desarrollado y testeado en las placas `Raspberry Pi 2 Model B`_ 
y `Raspberri Pi 3 model B`_, con el sistema operativo `Raspbian`_ release 9.1, codename 
`Stretch`_. Otras versiones de Raspberry o Raspbian podrían no funcionar correctamente.  

.. _Raspberri Pi 3 Model B: https://www.raspberrypi.org/products/raspberry-pi-3-model-b/
.. _Raspberry Pi 2 Model B: https://www.raspberrypi.org/products/raspberry-pi-2-model-b/
.. _Stretch: http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2019-04-09/2019-04-08-raspbian-stretch-lite.zip
.. _placas Raspberry: https://www.raspberrypi.org/products/
.. _Raspbian: https://www.raspberrypi.org/downloads/raspbian/
.. _raspberrypi.org: https://www.raspberrypi.org/downloads/
