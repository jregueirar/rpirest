====================
Módulos
====================

* apirest_dht
* apirest_sensehat
* apirest_mpd
* apirest_rpi
* apirest_gpio

Extendiendo la API REST: como crear un nuevo módulo
============

If you have python know and you know how the DRF works, you can extend
this Framework. You can add support to a new shield, sensor or gadget
connected to your Raspberry.

You only have to follow these steps:

- Create a new django application: python3 manage.py startapp apirest_<name_app>
- Create a API REST with DRF using the class MyRouter() from core.common
- Make the Rest API Available editing rpirest/urls.py using the router
created.

Here is an example, extracted from the apires_dht app code:

```python
### file apirest_dht/views.py

def routes():

    router = MyRouter()
    router.register(r'env_sensor/humidity', HumidityView, base_name='dht_humidity')
    router.register(r'env_sensor/temperature', TemperatureView, base_name='dht_temperature')
    return router.urls

#...

class TemperatureView(viewsets.ViewSet):
    """
    Gets the current temperature in degrees Celsius from the humidity sensor.
    Api rest of get_temperature_from_humidity.
    [ref]: https://pythonhosted.org/sense-hat/api/#environmental-sensors
    """
    def list(self, request, device):
        humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSORS[device], settings.DHT_GPIO_PIN)
        response = apirest_response_format(request=request, status="success", msg="Sensor " + device, result=temperature)
        return Response(response)

```

```python
### file rpirest/urls.py

#...
from apirest_dht.views import routes as dht_routes
#...
   
urlpatterns = [
    #...
    url(r'^api/v1/dht11/', include(dht_routes()), {'device': "dht11"}),
    #...
  
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
```

