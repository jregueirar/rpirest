Introducción
============

RPiREST es un framework que permite gestionar y supervisar de forma remota 
los recursos hardware propios que posee una Raspberry Pi, así como de los actuadores 
y sensores que se le pueden conectar siguiendo la filosofía IoT. 

RPiREST pretende ser "LA API REST navegable para tus
proyectos con una Raspberry Pi". Es decir es un 
software que se instala en una Raspberry Pi si se 
quiere afrontar un proyecto de IoT con este SBC haciendo uso de una
interfaz REST.

De este modo RPiREST posee las siguientes características como objetivo: 

  * Exponer en una red, en una Intranet o Internet, recursos hardware propios de una 
    Raspberry Pi así como funcionalidades de sensores y actuadores conectados. 

  * Facilidad de instalación, configuración y despliegue masivo en modelos de Raspberry Pi,
    siguiendo una metodología DevOps.

  * Facilidad de uso al ser una API Web REST navegable, documentada y directamente
    te usable desde un navegador web. Al ser navegable, el usuario final puede aprender 
    interactuando cuáles son los recursos que ofrece la API desde un navegador
    Web, cuáles son sus parámetros de entrada e incluso mandar órdenes desde sencillos formularios webs. 
    La API RESTful utiliza como formato de intercambio de
    datos JSON, con una respuesta que sigue un formato que informa no sólo del resultado, 
    sino que arroja más información que podría ser de utilidad.

  * Facilitar a un programador FrontEnd la creación de cuadro de mandos multidisposi-
    tivos, alejándole del backend, de tener que preocuparse de conocimientos hardware
    específicos.
    
  * Diseño modular para que sea sencillo dar soporte a un nuevo sensor o actuador y
    crear nuevas soluciones IoT como consecuencia de la mezcla de distintos componentes.
   
  * Uso de tecnologías estándares y actuales de modo que se pueda alcanzar a una
    amplia comunidad de usuarios.
    
  * Facilidades para gestión de usuarios y permisos.
    
  * Identificación y autorización segura para usuarios y para las comunicaciones.
    
  * Software Libre como requisito, con un repositorio de código y gestor de incidencias
    público, para invitar a hacer comunidad y que el proyecto mejore y siga creciendo
    con nuevas ideas y desarrollos.
