.. RpiREST documentation master file, created by
   sphinx-quickstart on Tue Apr  7 23:31:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RpiREST's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   introduction
   require
   installation
   modules
   user



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
