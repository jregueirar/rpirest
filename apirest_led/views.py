from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework import viewsets, status
from django.conf import settings
from core.common import MyRouter
from core.common import apirest_response_format, rpirest_response
from apirest_led.serializers import LedSerializer
import sys

import logging
logger = logging.getLogger("apirest_led")

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)


def routes():

    router = MyRouter()
    router.register(r'', LedView, base_name='led')
    return router.urls


# Devuelve un hash con la siguiente informacion:
# {
#   mode: "IN, OUT,..."
#   value:
# }
def pin_info(pin):
    result = {}
    func = GPIO.gpio_function(pin)
    logger.debug("Mode int: " + str(func))
    result['mode'] = gpiomode_int2str[func]
    logger.debug("Pin: " + str(pin) + ", Modo: " + result['mode'])

    if func is GPIO.OUT:
        GPIO.setup(pin, GPIO.OUT)
        result['value'] = GPIO.input(pin)
    else:
        result['value'] = "unknow"

    return result


class LedView(viewsets.ViewSet):
    lookup_value_regex = '[1-9]|[1-3][0-9]|40'
    serializer_class = LedSerializer

    def list(self, request):
        if request.query_params:
            logger.debug(request.query_params)
            serializer = LedSerializer(data=request.query_params)
        else:
            serializer = LedSerializer(data=request.data)

        if serializer.is_valid():
            try:
                data = {
                    "pin": serializer.data['pin'],
                    "on": self.led_is_lit(pin=serializer.data['pin'])
                 }
                return rpirest_response(request=request,
                                        status=status.HTTP_200_OK, result=data)
            except:
                return rpirest_response(request=request,
                                        status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                        result="---", detail=sys.exc_info()[1])
        else:
            return rpirest_response(request=request,
                                    status=status.HTTP_400_BAD_REQUEST,
                                    result="", detail=serializer.errors)

    def update(self, request, pk=None):
        # self.serializer_class = LedSerializer
        serializer = LedSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                info = self.led_on_off(serializer.data)
                return rpirest_response(request=request,
                                 status='success',
                                 result=info)
            except:
                return rpirest_response(request=request,
                                 status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                 result=None, detail=sys.exc_info()[1])
        else:
            return rpirest_response(request=request,
                             status=status.HTTP_400_BAD_REQUEST,
                             result="", detail=serializer.errors)

    def led_on_off(self, data):
        GPIO.setup(data['pin'], GPIO.OUT)
        GPIO.output(data['pin'], data['on'])

        if data['on']:
            return "LED ON"
        else:
            return "LED OFF"

    def led_is_lit(self, pin):
        GPIO.setup(pin, GPIO.OUT)
        if GPIO.input(pin):
            return True
        else:
            return False
