from django.apps import AppConfig


class ApirestLedConfig(AppConfig):
    name = 'apirest_led'
