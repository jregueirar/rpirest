from rest_framework import serializers


class LedSerializer(serializers.Serializer):
    pin = serializers.IntegerField(min_value=1, max_value=40)
    on = serializers.BooleanField()
